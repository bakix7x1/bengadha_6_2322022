/* eslint-disable no-unused-vars */
function mediaFactory (data, name) {
  const { title, image, likes, video } = data
  let ajLikes = likes

  const picture = `assets/photographers/${name.split(' ')[0]}/${image}`
  const movie = `assets/photographers/${name.split(' ')[0]}/${video}`

  function getMediaProfileDOM () {
    const section = document.createElement('a')
    section.href = '#'
    section.setAttribute('class', 'mediaprofile')

    if (image) {
      const img = document.createElement('img')
      img.setAttribute('src', picture)
      img.setAttribute('class', 'source-media')
      img.setAttribute('alt', 'image')
      section.appendChild(img)
    } else if (video) {
      const video = document.createElement('video')
      video.setAttribute('src', movie)
      video.setAttribute('class', 'source-media')
      section.appendChild(video)
    }

    const div = document.createElement('div')
    div.setAttribute('class', 'single-media-info')
    section.appendChild(div)
    const h4 = document.createElement('h4')
    h4.textContent = title
    h4.setAttribute('class', 'title-photos')
    div.appendChild(h4)
    const p = document.createElement('p')
    div.appendChild(p)
    const span = document.createElement('span')
    span.textContent = ajLikes
    p.appendChild(span)
    const button = document.createElement('button')
    button.setAttribute('class', 'like-btn')
    const i2 = document.createElement('i')
    i2.setAttribute('class', 'fas fa-heart')
    button.addEventListener('click', function (event) {
      event.preventDefault()
      event.stopPropagation()
      console.log('like')
      ajLikes++
      span.textContent = ajLikes
      const totalLikes = document.querySelector('.count-like')
      const total = Number(totalLikes.textContent)
      totalLikes.textContent = total + 1
    })
    button.appendChild(i2)
    p.appendChild(button)

    // eslint-disable-next-line no-undef
    section.addEventListener('click', (event) => {
      event.preventDefault()
      // eslint-disable-next-line no-undef
      lightbox(event.currentTarget)
    })

    // section.onclick = lightbox

    return section
  }

  return { title, picture, getMediaProfileDOM }
}
