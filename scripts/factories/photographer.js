// function affichant les photographe dans index

// eslint-disable-next-line no-unused-vars
function photographerFactory (data) {
  const { name, portrait, id, city, country, tagline, price } = data

  const picture = `assets/photographers/${portrait}`

  function getUserCardDOM () {
    const a = document.createElement('a')
    const article = document.createElement('article')
    const img = document.createElement('img')
    img.setAttribute('src', picture)
    const h2 = document.createElement('h2')
    h2.textContent = name
    article.appendChild(img)
    article.appendChild(h2)
    a.appendChild(article)
    const h3 = document.createElement('h3')
    h3.textContent = city + ', ' + country
    a.appendChild(h3)
    const p1 = document.createElement('p')
    const p2 = document.createElement('p')
    p1.setAttribute('class', 'p1')
    p2.setAttribute('class', 'p2')
    p1.textContent = tagline
    p2.textContent = price + '€ Par jours'
    a.appendChild(p1)
    a.appendChild(p2)
    a.href = 'photographer.html?id=' + id
    return (a)
  }
  return { name, picture, getUserCardDOM }
}

// function afficher le profil du photographe

// eslint-disable-next-line no-unused-vars
function profileFactory (data) {
  const { name, portrait, city, country, tagline } = data

  const picture = `assets/photographers/${portrait}`

  function getProfileDOM () {
    const section = document.createElement('section')
    section.setAttribute('class', 'photograph-header')
    const div = document.createElement('div')
    div.setAttribute('class', 'information_profile')
    const h2 = document.createElement('h2')
    h2.textContent = name
    div.appendChild(h2)
    section.appendChild(div)
    const h3 = document.createElement('h3')
    h3.textContent = city + ', ' + country
    div.appendChild(h3)
    const p = document.createElement('p')
    p.textContent = tagline
    div.appendChild(p)
    const button = document.createElement('button')
    button.setAttribute('class', 'contact_button')
    button.textContent = 'Contactez-moi'
    // eslint-disable-next-line no-undef
    button.onclick = displayModal
    section.appendChild(button)
    const img = document.createElement('img')
    img.setAttribute('src', picture)
    section.appendChild(img)
    return (section)
  }
  return { name, picture, getProfileDOM }
}

// function qui affiche les media du photographe
