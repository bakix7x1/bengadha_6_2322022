// recuperer les données json

async function getDataFishEye () {
  const url = '../data/photographers.json'
  const response = await fetch(url)
  const data = await response.json()
  return data
}

async function getAllPhotographers () {
  const data = await getDataFishEye()
  return data.photographers
}

// eslint-disable-next-line no-unused-vars
async function getMedias (id) {
  const data = await getDataFishEye()
  // eslint-disable-next-line eqeqeq
  const medias = data.media.filter(media => media.photographerId == id)
  return medias
}

// eslint-disable-next-line no-unused-vars
async function getOnePhotographer (id) {
  const data = await getAllPhotographers()
  // eslint-disable-next-line eqeqeq
  return data.find(photographer => id == photographer.id)
}
