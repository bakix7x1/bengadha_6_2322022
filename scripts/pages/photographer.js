// Mettre le code JavaScript lié à la page photographer.html

const url = new URL(window.location.href)
const idProfile = url.searchParams.get('id')
console.log(idProfile)

// eslint-disable-next-line no-unused-vars
async function getPhotographers () {
  // eslint-disable-next-line no-undef
  return await getAllPhotographers()
}

// eslint-disable-next-line no-unused-vars
const count = 0

// --affiche le profile du photographe

async function displayData (photographers) {
  const profileSection = document.querySelector('.photograph-profile')
  const spanPrice = document.querySelector('.price-aside')

  // eslint-disable-next-line no-undef
  const profileModel = profileFactory(photographers)
  const profileCardDOM = profileModel.getProfileDOM()
  profileSection.appendChild(profileCardDOM)
  spanPrice.innerHTML = photographers.price
  /* spanNameForm.innerHTML = photographer.name; */
};

// --affiche les media

async function displayMediaData (medias, name) {
  const mediasSection = document.querySelector('.media_section')
  const spanCountLike = document.querySelector('.count-like')
  const orderbySelect = document.querySelector('#orderby')

  let count = 0
  const mediaArray = []

  medias.forEach((media) => {
    // eslint-disable-next-line eqeqeq
    if (media.photographerId == idProfile) {
      mediaArray.push(media)
    }
  })

  console.log(mediaArray)

  // ---SYSTEME DE TRI

  orderbySelect.addEventListener('change', function () {
    // Tri par populariter

    // eslint-disable-next-line eqeqeq
    if (this.value == 'likes') {
      mediaArray.sort(function (a, b) {
        return b.likes - a.likes
      })
      mediasSection.innerHTML = ''
      affichageMedia()
      // eslint-disable-next-line no-undef
      // mediaLoading = []
      // eslint-disable-next-line no-undef
      // lightbox()

      // Tri par date
    // eslint-disable-next-line eqeqeq
    } else if (this.value == 'date') {
      mediaArray.sort(function (a, b) {
        return new Date(b.date) - new Date(a.date)
      })
      mediasSection.innerHTML = ''
      affichageMedia()
      // eslint-disable-next-line no-undef
      // mediaLoading = []
      // eslint-disable-next-line no-undef
      // lightbox()

      // Tri par alphabétique
    // eslint-disable-next-line eqeqeq
    } else if (this.value == 'alphabetique') {
      // eslint-disable-next-line array-callback-return
      mediaArray.sort(function (a, b) {
        if (a.title < b.title) {
          return -1
        }
      })
      mediasSection.innerHTML = ''
      affichageMedia()
      // eslint-disable-next-line no-undef
      // mediaLoading = []
      // eslint-disable-next-line no-undef
      // lightbox()
    }
  })
  affichageMedia()

  // --AFFICHAGE MEDIA

  function affichageMedia () {
    mediaArray.forEach((media) => {
      // eslint-disable-next-line no-undef
      const mediaModel = mediaFactory(media, name)
      const userCardDOM = mediaModel.getMediaProfileDOM()
      mediasSection.appendChild(userCardDOM)
      count += media.likes
    })
  }

  spanCountLike.innerHTML = count
};

async function init () {
  // Récupère les datas d'un photographes
  // eslint-disable-next-line no-undef
  const photographers = await getOnePhotographer(idProfile)
  displayData(photographers)

  // Récupère media via id du photographe
  // eslint-disable-next-line no-undef
  const medias = await getMedias(idProfile)
  displayMediaData(medias, photographers.name)
};

init()
