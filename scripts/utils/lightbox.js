// --LIGHTBOX

let indexMedia = 0
const mediaLoading = []
const titleLoading = []

// AFFICHAGE LIGHTBOX

// eslint-disable-next-line no-unused-vars
function lightbox (media) {
  const mediaSelector = document.querySelectorAll('.source-media')
  const lightboxSelector = document.querySelector('.lightbox')
  const lightboxSrc = document.querySelector('.lightbox-img')
  const lightboxVideo = document.querySelector('.lightbox__container > video')
  const lightboxTitle = document.querySelector('.lightbox-title')
  const titlePotos = document.querySelectorAll('.title-photos')

  console.log(media)
  const mediaSrc = media.querySelector('.source-media')
  console.log(mediaSrc)

  mediaSelector.forEach((source) => {
    mediaLoading.push(source.getAttribute('src'))
  })
  titlePotos.forEach((title) => {
    titleLoading.push(title.textContent)
  })
  const link = mediaSrc.getAttribute('src')
  if (link.includes('.jpg')) {
    lightboxSelector.style.display = 'block'
    lightboxSrc.style.display = 'block'
    lightboxSrc.setAttribute('src', link)
    lightboxVideo.style.display = 'none'
  } else if (link.includes('.mp4')) {
    lightboxSelector.style.display = 'block'
    lightboxSrc.style.display = 'none'
    lightboxVideo.style.display = 'block'
    lightboxVideo.setAttribute('src', link)
  }
  indexMedia = mediaLoading.findIndex((media) => media === link)
  lightboxTitle.textContent = titleLoading[indexMedia]

  lightboxController()
}

//  FONCTION CONTROLE DE LA LIGHTBOX

function lightboxController () {
  const lightboxSelector = document.querySelector('.lightbox')
  const lightboxSrc = document.querySelector('.lightbox-img')
  const lightboxClose = document.querySelector('.lightbox__close')
  const lightboxNext = document.querySelector('.lightbox__next')
  const lightboxPrev = document.querySelector('.lightbox__prev')
  const lightboxVideo = document.querySelector('.lightbox-video')
  const lightboxTitle = document.querySelector('.lightbox-title')

  // -------- Utils Function to Lightbox Controller ---------------------

  function conditionNextPrev () {
    if (mediaLoading[indexMedia].includes('.jpg')) {
      lightboxSelector.style.display = 'block'
      lightboxSrc.style.display = 'block'
      lightboxSrc.setAttribute('src', mediaLoading[indexMedia])
      lightboxVideo.style.display = 'none'
    } else if (mediaLoading[indexMedia].includes('.mp4')) {
      lightboxSelector.style.display = 'block'
      lightboxSrc.style.display = 'none'
      lightboxVideo.style.display = 'block'
      lightboxVideo.setAttribute('src', mediaLoading[indexMedia])
    }
    lightboxTitle.textContent = titleLoading[indexMedia]
  }

  function nextIndexMedia () {
    if (indexMedia < mediaLoading.length - 1) {
      indexMedia++
    }
  }

  function prevIndexMedia () {
    if (indexMedia > 0) {
      indexMedia--
    } else {
      indexMedia = mediaLoading.length - 1
    }
  }
  // -------------------------------------------------------------

  // Fleche suivante Lightbox
  lightboxNext.addEventListener('click', function (e) {
    e.preventDefault()
    nextIndexMedia()
    conditionNextPrev()
  })

  // Fleche précédent Lightbox

  lightboxPrev.addEventListener('click', function (e) {
    e.preventDefault()
    prevIndexMedia()
    conditionNextPrev()
  })

  // Fermeture Ligthbox
  lightboxClose.addEventListener('click', function () {
    lightboxSelector.style.display = 'none'
  })

  // Controller Lightbox clavier

  window.addEventListener('keyup', (event) => {
    if (event.key === 'ArrowLeft') {
      prevIndexMedia()
      conditionNextPrev()
    }
    if (event.key === 'ArrowRight') {
      nextIndexMedia()
      conditionNextPrev()
    }

    if (event.key === 'Escape') {
      lightboxSelector.style.display = 'none'
    }
  })
}
